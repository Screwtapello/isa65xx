#!/bin/sh
set -eux

die() { printf "%s\n" "$*" >&2; exit 1; }

cd "$(dirname "$0")"
rm -rf isa65xx.sqlite3

# Load the database without foreign key constraints enabled.
sqlite3 isa65xx.sqlite3 -bail -init isa65xx.sql ""

# Now let's check for foreign key errors!
sqlite3 -tabs isa65xx.sqlite3 -init /dev/null '
SELECT
    fkc."table" AS "from_table",
    fkl."from" AS "from_field",
    fkc.rowid AS "from_rowid",
    fkl."table" AS "to_table",
    fkl."to" AS "to_field"
FROM pragma_foreign_key_check() AS fkc
    JOIN pragma_foreign_key_list(fkc."table") AS fkl
WHERE fkc.fkid = fkl.id;
' > foreign-key-errors.tsv

if [ -s foreign-key-errors.tsv ]; then
    cat foreign-key-errors.tsv >&2
    die "Foreign key errors found!"
fi

# Make sure no two opcodes have the same pattern.
sqlite3 -tabs isa65xx.sqlite3 -init /dev/null '
SELECT pattern
FROM opcodes
GROUP BY pattern
HAVING count(*) > 1
' > opcode-duplicate-patterns.tsv
if [ -s opcode-duplicate-patterns.tsv ]; then
    cat opcode-duplicate-patterns.tsv >&2
    die "Duplicate patterns found"
fi

# Save the computed opcode table to a file so we can examine it.
sqlite3 -tabs -header isa65xx.sqlite3 -init /dev/null \
    "SELECT * FROM opcodes ORDER BY opcode" > opcodes_generated.tsv

# Is it what we expected?
diff -ur opcodes_expected.tsv opcodes_generated.tsv ||
    die "Unexpected difference found!"
