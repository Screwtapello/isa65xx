-- This query formats opcodes and addressing modes in the same order as
-- Appendix B of the Synertek 6502 Programming Manual.
SELECT
    mnemonic,
    mode,
    format('%02X', opcode) as opcode,
    bytes,
    coalesce(force_cycles, base_cycles) as cycles
FROM opcodes
ORDER BY mnemonic,
    CASE mode
        WHEN 'Accumulator' THEN 10
        WHEN 'Immediate' THEN 20
        WHEN 'ZeroPage' THEN 30
        WHEN 'ZeroPageIndexedWithX' THEN 40
        WHEN 'ZeroPageIndexedWithY' THEN 50
        WHEN 'Absolute' THEN 60
        WHEN 'AbsoluteIndexedWithX' THEN 70
        WHEN 'AbsoluteIndexedWithY' THEN 80
        WHEN 'Implied' THEN 90
        WHEN 'ProgramCounterRelative' THEN 100
        WHEN 'ZeroPageIndexedIndirect' THEN 110
        WHEN 'ZeroPageIndirectIndexedWithY' THEN 120
        WHEN 'AbsoluteIndirect' THEN 130
        ELSE 999
    END;

-- These queries format opcodes and addressing modes in the same order
-- as http://www.6502.org/tutorials/65c02opcodes.html
SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE mode = 'ZeroPageIndirect'
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE opcode in (0x89, 0x34, 0x3C)
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE opcode in (0x3A, 0x1A)
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE opcode=0x7C
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE opcode=0x80
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE opcode in (0xDA, 0x5A, 0xFA, 0x7A)
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE mnemonic = 'STZ'
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE mnemonic = 'TRB'
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE mnemonic = 'TSB'
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE opcode in (0x1E, 0x5E, 0x3E, 0x7E)
ORDER BY pattern;

SELECT
    format('%02X', opcode) as OP,
        bytes as LEN,
        coalesce(force_cycles, base_cycles) as CYC,
        mode as MODE,
        pattern as SYNTAX
FROM opcodes
WHERE opcode = 0x6C
ORDER BY pattern;
