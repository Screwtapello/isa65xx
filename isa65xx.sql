-- Generating the 6502 Instruction Set
--------------------------------------
--
-- Like any CPU, the 6502 can perform many different operations.
-- Each operation is given an "operation code" (or "opcode"), a number
-- from 0-255 which tells the processor which operation to perform.
-- However, these opcodes aren't entirely random, there's a certain amount
-- of structure to them.  While you could implement a 6502 emulator
-- that just hard-coded every possible operation (and I'm sure pepole
-- have), that would make it difficult to discover and debug typos and
-- copy/paste errors.  Instead, we will attempt to record as little data
-- as possible, validate that data as strictly as we can, and synthesise
-- the resulting opcode list.  That will make it much easier to verify
-- the data and trust the results.
--
-- (Note: there is a bibliography at the end of this document)
--
-- We start by dividing operations into "instructions" and "addressing
-- modes".  For example, take the operations "LDA #$12", "LDA $1234",
-- and "STA $1234".  The operations beginning with "LDA" both "LoaD
-- the Accumulator" with a value, but one uses "#" to signify that
-- the value is stored immediately after the opcode (hexadecimal 12,
-- written $12 to distinguish it from decimal values), while the other
-- loads a value from memory at hexadecimal address $1234.  Likewise,
-- both the operations ending with "$1234" refer to the same memory
-- address, but one loads the value at that address, while the other
-- stores the value in the accumulator into that address.
--
-- Already we have two instructions ("LDA" and "STA") and two addressing
-- modes ("#$12", the immediate mode, and "$1234", the absolute address
-- mode).
--
-- However, not every instruction can be used with every instruction.
-- For example, LDA can be used with the immediate mode but STA cannot
-- - it wouldn't make sense to store a value in the middle of the
-- instruction stream.  Before we get into the details of instructions
-- and addressing modes, we need to talk about instruction *kinds*.
--
-- Instruction Kinds
--------------------
--
-- No 6502 source that I'm aware of uses this specific model, but
-- the documentation talks about special behaviour for instructions
-- that write to memory, or that read-modify-write memory.  Therefore,
-- it seems reasonable to divide up the 6502 instructions into these
-- categories.  These categories tell us two things: they suggest what
-- addressing modes can be used with an instruction, and also (because
-- the 6502 makes one memory access per CPU cycle) how many CPU cycles
-- the instruction will take to execute.
--
-- Our table of instruction kinds is just a set of names and cycle counts.
CREATE TABLE instruction_kinds (
    instruction_kind TEXT NOT NULL PRIMARY KEY,
    cycles INTEGER NOT NULL
) STRICT;

-- The W65C02S data sheet mentions "read-modify-write" instructions as
-- a special case, and also mentions "STA $hhhh,X" as a special case.
-- "The 6502 Instruction Set Decoded" mentions "instructions that write"
-- (including STA) as a special case.  With those cases in mind, and
-- looking at the list of 6502 instructions, the following groups seem
-- to be important.
INSERT INTO instruction_kinds VALUES

-- "Implied" is for instructions that do not interact with the operand
-- provided by the addressing mode (if any).  They may interact with
-- memory, like PLA and PHA, but the memory address and the value come
-- from elsewhere.
--
-- Generally, they do not take any additional cycles.
('Implied', 0),

-- "Address" is for instructions that require a 16-bit address operand,
-- like JMP and JSR.
--
-- Generally, they do not take any additional cycles.
('Address', 0),

-- "Read" is for instructions that require an 8-bit value operand,
-- like LDA or SBC.
--
-- Generally, they take an additional cycle to read the value from memory.
('Read', 1),

-- "Write" is for instructions that that need a place to put an 8-bit
-- value, like STA, STX, and STY.
--
-- Generally, they take an additional cycle to write the value to memory.
('Write', 1),

-- "ReadModifyWrite" is for instructions that read an 8-bit value,
-- modify it, then write it back to the same location, like ASL, LSR,
-- ROL, ROR, INC, and DEC.
--
-- Generally, they take an additional cycle to read the value, to modify
-- it, and to write it back.
('ReadModifyWrite', 3);

-- Instructions
---------------
--
-- Now that we know what kinds of instruction the 6502 supports, let's
-- record the complete list.
--
-- Here's the schema we'll use to represent instructions:
CREATE TABLE instructions (

    -- The human-readable abbreviation for this instruction, such as
    -- "LDA" or "ADC".
    mnemonic TEXT NOT NULL CHECK (
        substring(mnemonic, 1, 1) BETWEEN 'A' AND 'Z' AND
        substring(mnemonic, 2, 1) BETWEEN 'A' AND 'Z' AND
        substring(mnemonic, 3, 1) BETWEEN 'A' AND 'Z' AND
        length(mnemonic) == 3
    ),

    -- Sometimes two different behaviour (i.e. two different instructions)
    -- share a mnemonic, so the discriminator lets us tell them apart.
    discriminator TEXT NOT NULL DEFAULT '',

    -- The kind of instruction this is, from the instruction_kinds
    -- table above.
    instruction_kind TEXT NOT NULL REFERENCES instruction_kinds,

    -- Some instructions require extra CPU cycles to perform their task,
    -- beyond just interacting with memory.  When that is the case,
    -- it can be recorded here.
    base_cycles INTEGER NOT NULL CHECK (base_cycles >= 0),

    -- Some instructions can take a variable number of cycles depending on
    -- the specific data they process.  For example, branch instructions
    -- take an extra cycle if the branch is taken.  When that is the case,
    -- the maximum number of extra cycles can be recorded here.
    extra_cycles INTEGER NOT NULL CHECK (extra_cycles >= 0),

    -- An instruction is identified by its mnemonic and discriminator
    -- (if any).
    PRIMARY KEY (mnemonic, discriminator),

    -- Even though we have a primary key on the mnemonic and discriminator
    -- fields guaranteeing uniqueness, we will want to reference the
    -- mnemonic, discriminator, and kind from other tables later, so
    -- we'll need a UNIQUE constraint on them.
    UNIQUE (mnemonic, discriminator, instruction_kind)
) STRICT;

-- The list of instructions comes from "Table 5-1: Instruction Set Table"
-- in the W65C02S data sheet.  The instruction kinds are derived from
-- the operation descriptions in "Table 6-4 Operation, Operations Codes
-- and Status Register".  The cycle counts citations are described for
-- each instruction where they're given.
INSERT INTO instructions VALUES

-- ADC takes an extra cycle in decimal mode on the 65C02
('ADC', '',          'Read',            0, 1),

('AND', '',          'Read',            0, 0),
('ASL', '',          'ReadModifyWrite', 0, 0),

-- Branch instructions take an extra cycle if the branch is taken
-- (accounted for here) and a second extra cycle if the target address
-- crosses a page boundary (accounted for in the ProgramCounterRelative
-- addressing mode).
('BCC', '',          'Address',         0, 1),
('BCS', '',          'Address',         0, 1),
('BEQ', '',          'Address',         0, 1),

-- The BIT instruction on the 6502 in all addressing modes, and the
-- indexed addressing modes added in the 65C02, reads a value from memory
-- then sets the Negative and oVerflow flags according to bits 7 and 6
-- of the value, and the Zero flag according to whether the bitwise-AND
-- of the value and the accumulator is zero.
--
-- The immediate-mode version of BIT, also added in the 65C02, is
-- different.  It does not modify the Negative or oVerflow flags (since
-- it takes an immediate value, the programmer already knows whether
-- bits 7 or 6 are set), and only does the bitwise-AND calculation.
-- Since it behaves differently, we need to treat it as a different
-- instruction, so we'll give it a discriminator.
('BIT', '',          'Read',            0, 0),
('BIT', 'immediate', 'Read',            0, 0),

-- Branch instructions take an extra cycle if the branch is taken
-- (accounted for here) and a second extra cycle if the target address
-- crosses a page boundary (accounted for in the ProgramCounterRelative
-- addressing mode).
('BMI', '',          'Address',         0, 1),
('BNE', '',          'Address',         0, 1),
('BPL', '',          'Address',         0, 1),

-- The BRA instruction always takes the branch, so it always incurs
-- the "optional" cycle penalty for that, but there's still the other
-- optional cycle penalty for crossing a page boundary (accounted for
-- in the ProgramCounterRelative addressing mode).
('BRA', '',          'Address',         1, 0),

-- Usually, BRK is treated as having the 'Implied' addressing mode, since
-- it doesn't need any extra information to do its job.  However, when
-- the interrupt handler that BRK invokes returns, the program counter
-- is set to the byte *after* the byte after the BRK - it's handled
-- like it has a one-byte payload, like an 'Immediate' mode instruction.
-- So rather than have a special case in the 'Implied' addressing mode
-- just for this instruction, we'll just make it 'Immediate'.
--
-- As a result, we need to give it the 'Read' instruction kind, instead of
-- 'Implied'.
--
-- The Synertek 6502 Programming Manual says that BRK takes seven cycles.
-- Since we give BRK the "Immediate" addressing mode, which we model
-- as taking 1 cycle, and Read implies 1 cycle, we say BRK takes 5
-- extra cycles.
('BRK', '',          'Read',            5, 0),

-- Branch instructions take an extra cycle if the branch is taken
-- (accounted for here) and a second extra cycle if the target address
-- crosses a page boundary (accounted for in the ProgramCounterRelative
-- addressing mode).
('BVC', '',          'Address',         0, 1),
('BVS', '',          'Address',         0, 1),

('CLC', '',          'Implied',         0, 0),
('CLD', '',          'Implied',         0, 0),
('CLI', '',          'Implied',         0, 0),
('CLV', '',          'Implied',         0, 0),
('CMP', '',          'Read',            0, 0),
('CPX', '',          'Read',            0, 0),
('CPY', '',          'Read',            0, 0),
('DEC', '',          'ReadModifyWrite', 0, 0),
('DEX', '',          'Implied',         0, 0),
('DEY', '',          'Implied',         0, 0),
('EOR', '',          'Read',            0, 0),
('INC', '',          'ReadModifyWrite', 0, 0),
('INX', '',          'Implied',         0, 0),
('INY', '',          'Implied',         0, 0),

-- In the traditional 6502 assembler notation, there's a JMP instruction
-- that can be used with the "absolute" addressing mode, or the "absolute
-- indirect" addressing mode (which isn't used by any other instruction).
-- However, studying the 6502 instruction encoding (as seen in The 6502
-- Instruction Set Decoded) it seems there's actually only one addressing
-- mode ("absolute") but two JMP instructions: one that uses the given
-- address as-is, and an "indirect JMP" that reads the given address to
-- find the real target.  The 65C02 adds the "absolute indexed indirect"
-- addressing mode for JMP, but that too is just the "absolute indexed
-- with X" mode used with the "indirect JMP" instruction.
--
-- To save on implementing extra addressing modes, let's separate out
-- the indirect JMP instruction.
--
-- Note that the indirect JMP takes two extra cycles to read the target
-- memory address.  The 6502 Instruction Set Decoded says (of the 65C02):
-- "JMP (absolute) spends an extra cycle making sure the high byte of the
-- indirect address is correct (this fixes the famous 6502 indirect jump
-- bug)" and also "JMP (absolute,X) spends a cycle doing the indexing.
-- It doesn't need to spend an additional cycle getting the high byte
-- right".  As a result, both addressing modes for the indirect JMP take
-- one additional cycle, for a total of 3.
('JMP', '',          'Address',         0, 0),
('JMP', 'indirect',  'Address',         3, 0),

-- JSR spends two cycles pushing the return address to the stack.
-- "The 6502 Instruction Set Decoded" says it "spends an extra cycle
-- juggling the return address internally", for a total of 3 extra cycles.
('JSR', '',          'Address',         3, 0),

('LDA', '',          'Read',            0, 0),
('LDX', '',          'Read',            0, 0),
('LDY', '',          'Read',            0, 0),
('LSR', '',          'ReadModifyWrite', 0, 0),
('NOP', '',          'Implied',         0, 0),
('ORA', '',          'Read',            0, 0),

-- Pushing a byte to the stack takes an extra cycle.
('PHA', '',          'Implied',         1, 0),
('PHP', '',          'Implied',         1, 0),
('PHX', '',          'Implied',         1, 0),
('PHY', '',          'Implied',         1, 0),

-- Pulling a byte from the stack takes a cycle.  "The 6502 Instruction
-- Set Decoded" says "Instructions that pull data off the stack (PLA,
-- PLP, RTI, RTS) need an extra cycle to increment the stack pointer
-- (because the stack pointer points to the first empty address on the
-- stack, not the last used address)" for a total of two extra cycles.
('PLA', '',          'Implied',         2, 0),
('PLP', '',          'Implied',         2, 0),
('PLX', '',          'Implied',         2, 0),
('PLY', '',          'Implied',         2, 0),

('ROL', '',          'ReadModifyWrite', 0, 0),
('ROR', '',          'ReadModifyWrite', 0, 0),

-- RTI reads the processor status from the stack (one extra cycle) and
-- return address (two more cycles).  "The 6502 Instruction Set Decoded"
-- says "Instructions that pull data off the stack (PLA, PLP, RTI, RTS)
-- need an extra cycle to increment the stack pointer (because the stack
-- pointer points to the first empty address on the stack, not the last
-- used address)" for a total of four extra cycles.
('RTI', '',          'Implied',         4, 0),

-- RTS reads the return address from the stack (two extra cycles).
-- "The 6502 Instruction Set Decoded" says "Instructions that pull data
-- off the stack (PLA, PLP, RTI, RTS) need an extra cycle to increment
-- the stack pointer (because the stack pointer points to the first empty
-- address on the stack, not the last used address)" (one more cycle)
-- and also "RTS needs an extra cycle (in addition to the single-byte
-- penalty and the pull-from-stack penalty) to increment the return
-- address" for a total of four extra cycles.
('RTS', '',          'Implied',         4, 0),

-- SBC takes an extra cycle in decimal mode on the 65C02
('SBC', '',          'Read',            0, 1),

('SEC', '',          'Implied',         0, 0),
('SED', '',          'Implied',         0, 0),
('SEI', '',          'Implied',         0, 0),
('STA', '',          'Write',           0, 0),

-- STP is a 65816 instruction

('STX', '',          'Write',           0, 0),
('STY', '',          'Write',           0, 0),
('STZ', '',          'Write',           0, 0),
('TAX', '',          'Implied',         0, 0),
('TAY', '',          'Implied',         0, 0),
('TRB', '',          'ReadModifyWrite', 0, 0),
('TSB', '',          'ReadModifyWrite', 0, 0),
('TSX', '',          'Implied',         0, 0),
('TXA', '',          'Implied',         0, 0),
('TXS', '',          'Implied',         0, 0),
('TYA', '',          'Implied',         0, 0);

-- WAI is a 65816 instruction

-- Addressing Modes
-------------------
--
-- Now we have our instructions sorted out, we can turn to recording
-- the list of addressing modes.
--
-- Here's the schema we'll use to record them:
CREATE TABLE addressing_modes (

    -- The name of this mode, it should be an identifier and not empty
    mode TEXT NOT NULL PRIMARY KEY CHECK (mode <> ''),

    -- The minimum number of CPU cycles this addresssing mode adds to
    -- the operation execution.
    --
    -- The cycle count for an addressing mode should be 1-7 cycles.
    base_cycles INTEGER NOT NULL CHECK (base_cycles > 0 AND base_cycles < 8),

    -- Some modes will take additional cycles under certain conditions,
    -- for example indexed modes usually add a cycle if the new address
    -- crosses a page boundary.  When that is the case, the maximum
    -- number of extra cycles can be recorded here.
    extra_cycles INTEGER NOT NULL CHECK (extra_cycles >= 0),

    -- The number of bytes required to represent an operation with
    -- this mode.
    --
    -- An operation takes 1-3 bytes.
    bytes INTEGER NOT NULL CHECK (bytes > 0 AND bytes < 4),

    -- A pattern for formatting the mode in a disassembler, or
    -- possibly parsing it in an assembler.  "hh" means two hex digits,
    -- "hhhh" means four hex digits, "r" means a signed decimal value
    -- representing a distance in bytes
    --
    -- These patterns are not defined anywhere in particular, they
    -- just seem to be conventions for 6502 assembly that are commonly
    -- followed.
    pattern TEXT NOT NULL,

    -- The pattern of each addressing mode must include a big enough to
    -- format/parse its payload.
    CHECK (
        CASE bytes
            WHEN 1 THEN (pattern = '')
            WHEN 2 THEN (pattern LIKE '%hh%') OR (pattern LIKE '%r%')
            WHEN 3 THEN (pattern LIKE '%hhhh%')
            ELSE 0
        END
    )
) STRICT;

-- This information comes mostly from "Table 4-1: Addressing Mode Table"
-- in the W65C02S data sheet, although some modes have been changed.
-- The changes, along with citations justifying them, are described next
-- to the modes where they occur.
--
-- In general, these cycle counts are one less than the counts in the
-- W65C02S data sheet.  An instruction like LDA $1234 is of the "Read"
-- instruction kind, and the "Absolute" addressing mode.  It takes four
-- cycles, because it spends three cycles reading the instruction then
-- a fourth cycle reading that memory location.  Meanwhile, JMP $1234
-- ("Address" instruction kind, "Absolute" addressing mode) takes only
-- three cycles, since it doesn't read from that memory location, it
-- just loads the address into the program counter.
--
-- The W65C02S data sheet records the "Absolute" mode as taking four
-- cycles, which includes the memory read, which is correct for LDA but
-- incorrect for JMP.  Instead, we record the "Absolute" mode as three
-- cycles, the "Read" instruction kind as one cycle, and the "Address"
-- instruction kind as zero cycles, making our cycle counts correct.
INSERT INTO addressing_modes VALUES

('Absolute',                     3, 0, 3, '$hhhh'),

-- The "AbsoluteIndexedIndirect" mode is actually just the
-- AbsoluteIndexedWithX mode, used with the "JMP indirect" instruction,
-- so it doesn't need to be handled separately.

-- If adding X crosses a page boundary, this takes an extra cycle.
-- According to "The 6502 Instruction Set Decoded", an instruction
-- that writes always takes the extra cycle, whether or not it crosses
-- a boundary.
('AbsoluteIndexedWithX',         3, 1, 3, '$hhhh,X'),

-- If adding Y crosses a page boundary, this takes an extra cycle.
-- According to "The 6502 Instruction Set Decoded", an instruction
-- that writes always takes the extra cycle, whether or not it crosses
-- a boundary.
('AbsoluteIndexedWithY',         3, 1, 3, '$hhhh,Y'),

-- The "AbsoluteIndirect" mode is actually just the Absolute mode,
-- used with the "JMP indirect" instruction, so it doesn't need to be
-- handled separately.

-- "Accumulator" is similar to "Implied" - they have the same cycle-count,
-- byte-count, and pattern.  Many instructions that use the "Implied"
-- addressing mode even use the accumulator (like TXA or PLA) - but those
-- instructions *always* use the accumulator.  There are some instructions
-- that can be used with either the accumulator or memory, just like some
-- instructions can be used with either an immediate value or memory.
-- For the benefit of those instructions, we have the "Accumulator" mode.
--
-- Even though the pattern for this mode is the same as for the "Implied"
-- mode, there are no instructions that can use both, so it's still
-- unambiguous.
('Accumulator',                  2, 0, 1, ''),

('Immediate',                    1, 0, 2, '#$hh'),
('Implied',                      2, 0, 1, ''),

-- If the branch is taken, this takes an extra cycle (accounted for in
-- the instructions table above).  If the new address crosses a page
-- boundary, this takes a second extra cycle (accounted for here).
('ProgramCounterRelative',       2, 1, 2, '#r'),

-- "Stack" is not a very useful addressing mode, since stack instructions
-- vary wildly in cycle count, and don't really have much in common.
-- Since they each have unique behaviour, they need to be hardcoded,
-- so they might as well be recorded as "Implied".

('ZeroPage',                     2, 0, 2, '$hh'),

-- The ZeroPageIndexed* modes cannot cross a page boundary, they are
-- defined to wrap around inside the zero page, so there's no risk of
-- them taking an extra cycle.
('ZeroPageIndexedIndirect',      5, 0, 2, '($hh,X)'),
('ZeroPageIndexedWithX',         3, 0, 2, '$hh,X'),
('ZeroPageIndexedWithY',         3, 0, 2, '$hh,Y'),

-- Added in the 65C02.
('ZeroPageIndirect',             4, 0, 2, '($hh)'),

-- If adding Y crosses a page boundary, this takes an extra cycle.
-- According to "The 6502 Instruction Set Decoded", an instruction
-- that writes always takes the extra cycle, whether or not it crosses
-- a boundary.
('ZeroPageIndirectIndexedWithY', 4, 1, 2, '($hh),Y');

-- Now we have our list of addressing modes, we need to record which
-- modes are compatible with each instruction kind.
CREATE TABLE modes_for_instruction_kind (

    -- When an instruction wants this kind of informaiton...
    instruction_kind TEXT NOT NULL REFERENCES instruction_kinds,

    -- ...it can be provided by this addressing mode.
    mode TEXT NOT NULL REFERENCES addressing_modes,

    PRIMARY KEY (instruction_kind, mode)
) STRICT;

-- This information was derived by examining the modes used for each
-- instruction in "Table 6-4 Operation, Operation Codes and Status
-- Register" in the W65C02S data sheet, as well as the mapping from
-- instructions to instruction kinds in the "instructions" table above.
INSERT INTO modes_for_instruction_kind VALUES

-- An instruction that expects an implied value, requires an implied
-- value.
('Implied',         'Implied'),

-- An instruction that expects a 16-bit address, requires a 16-bit
-- address.
--
-- Although there's lots of modes that produce an address, the "Address"
-- instruction kind is only used by instructions like JMP, JSR, and
-- branches, so there's only a few permitted modes.
('Address',         'Absolute'),
('Address',         'AbsoluteIndexedWithX'),
('Address',         'ProgramCounterRelative'),

-- An instruction that reads an 8-bit value, can get it from an immediate
-- value or by reading the value at a 16-bit address.
('Read',            'Absolute'),
('Read',            'AbsoluteIndexedWithX'),
('Read',            'AbsoluteIndexedWithY'),
('Read',            'Immediate'),
('Read',            'ZeroPage'),
('Read',            'ZeroPageIndexedIndirect'),
('Read',            'ZeroPageIndexedWithX'),
('Read',            'ZeroPageIndexedWithY'),
('Read',            'ZeroPageIndirect'),
('Read',            'ZeroPageIndirectIndexedWithY'),

-- An instruction that writes an 8-bit value, can write it to a 16-bit
-- address.
('Write',           'Absolute'),
('Write',           'AbsoluteIndexedWithX'),
('Write',           'AbsoluteIndexedWithY'),
('Write',           'ZeroPage'),
('Write',           'ZeroPageIndexedIndirect'),
('Write',           'ZeroPageIndexedWithX'),
('Write',           'ZeroPageIndexedWithY'),
('Write',           'ZeroPageIndirect'),
('Write',           'ZeroPageIndirectIndexedWithY'),

-- An instruction that does read-modify-write can write to the
-- accumulator, or to a 16-bit address.
('ReadModifyWrite', 'Absolute'),
('ReadModifyWrite', 'AbsoluteIndexedWithX'),
('ReadModifyWrite', 'Accumulator'),
('ReadModifyWrite', 'ZeroPage'),
('ReadModifyWrite', 'ZeroPageIndexedWithX');

-- Decodable opcodes
--------------------
--
-- Now that we have definitions of all the instructions and addressing
-- modes we will encounter, we can start thinking about how to match
-- them up.  Most of the 6502's opcodes are "decodable"; that is,
-- certain bits reliably map to a particular instruction and other bits
-- reliably map to a particular addressing mode, so you can generate all
-- the possible combinations.  Not every opcode can be decoded like this
-- (some opcodes were put wherever they'd fit rather than according to
-- a pattern) but most of them are.
--
-- My reference for opcode decoding is the excellent page, "The 6502
-- Instruction Set Decoded", which extracts as many patterns as possible
-- out of the instruction set.  It's a bit more extensive than I need,
-- since at a certain point it's easier to hard-code a few instructions
-- that technically follow a pattern than it is to hard-code all the
-- exceptions to that pattern, but that's where this information comes
-- from.
--
-- For the opcodes that can be decoded, the two least significant bits
-- of the opcode are treated as a "group" of opcodes.  Within a group,
-- the top three bits indicate an instruction, and the remaining three
-- bits indicate an addressing mode - the instruction codes and mode
-- codes differ from group to group.
--
-- We'll start by recording the addressing-mode codes for each group.
CREATE TABLE decodable_addressing_modes (
    -- A group of opcodes for which these modes are valid
    opcode_group INTEGER NOT NULL
        -- A group is a 2-bit integer
        CHECK (opcode_group >= 0 AND opcode_group <= 2),

    -- An addressing mode code within this group is a 3-bit integer
    code INTEGER NOT NULL CHECK (code >= 0 AND code <= 7),

    -- The addressing mode that this code represents within this group
    mode TEXT NOT NULL REFERENCES addressing_modes,

    PRIMARY KEY (opcode_group, code)
) STRICT;

INSERT INTO decodable_addressing_modes VALUES
(0, 0, 'Immediate'),
(0, 1, 'ZeroPage'),
-- (0, 2) is not allocated
(0, 3, 'Absolute'),
-- (0, 4) is not allocated
(0, 5, 'ZeroPageIndexedWithX'),
-- (0, 6) is not allocated
(0, 7, 'AbsoluteIndexedWithX'),
(1, 0, 'ZeroPageIndexedIndirect'),
(1, 1, 'ZeroPage'),
(1, 2, 'Immediate'),
(1, 3, 'Absolute'),
(1, 4, 'ZeroPageIndirectIndexedWithY'),
(1, 5, 'ZeroPageIndexedWithX'),
(1, 6, 'AbsoluteIndexedWithY'),
(1, 7, 'AbsoluteIndexedWithX'),
(2, 1, 'ZeroPage'),
(2, 2, 'Accumulator'),
(2, 3, 'Absolute'),
-- (2, 4) is not allocated
(2, 5, 'ZeroPageIndexedWithX'),
-- (2, 6) is not allocated
(2, 7, 'AbsoluteIndexedWithX');

-- Meanwhile, here are the codes that represent different instructions
-- in different groups.
CREATE TABLE decodable_instructions (
    -- A group of opcodes for which these instructions are valid
    opcode_group INTEGER NOT NULL
        -- A group is a 2-bit integer
        CHECK (opcode_group >= 0 AND opcode_group <= 2),

    -- An instruction code within this group is a 3-bit integer
    code INTEGER NOT NULL CHECK (code >= 0 AND code <= 7),

    -- The instruction that this code represents within this group
    mnemonic TEXT NOT NULL,
    discriminator TEXT NOT NULL,

    PRIMARY KEY (opcode_group, code),
    FOREIGN KEY (mnemonic, discriminator) REFERENCES instructions
) STRICT;

INSERT INTO decodable_instructions VALUES

-- (0, 0) is not allocated

(0, 1, 'BIT', ''),

-- (0, 2) is 'JMP', but it's only valid with one addressing mode, so
-- there's no point trying to decode it with a pattern.

-- (0, 3) is 'JMP ($1234)' (indirect JMP), but it's also only valid with
-- one addressing mode on the 6502, and one more on the 65C02.

(0, 4, 'STY', ''),
(0, 5, 'LDY', ''),
(0, 6, 'CPY', ''),
(0, 7, 'CPX', ''),
(1, 0, 'ORA', ''),
(1, 1, 'AND', ''),
(1, 2, 'EOR', ''),
(1, 3, 'ADC', ''),
(1, 4, 'STA', ''),
(1, 5, 'LDA', ''),
(1, 6, 'CMP', ''),
(1, 7, 'SBC', ''),
(2, 0, 'ASL', ''),
(2, 1, 'ROL', ''),
(2, 2, 'LSR', ''),
(2, 3, 'ROR', ''),
(2, 4, 'STX', ''),
(2, 5, 'LDX', ''),
(2, 6, 'DEC', ''),
(2, 7, 'INC', '');

-- By joining the decodable_instructions and decodable_addressing_modes
-- tables together, you can generate nearly 75% of the opcodes that
-- the 6502 can execute.  However, you also generate a few instructions
-- that the 6502 does not support, or that behave differently than the
-- above patterns would lead you to expect, so we'll also need a list
-- of ignorables - opcodes that the above data produce, but which we
-- shouldn't include in our list of valid opcodes.
CREATE TABLE decodable_ignorables (
    -- An opcode generated by the decoding patterns that we don't want
    -- to generate
    opcode INTEGER NOT NULL PRIMARY KEY CHECK (opcode >= 0 AND opcode <= 255)
) STRICT;

-- These codes largely come from the blank cells in the opcode tables in
-- "The 6502 Instruction Set Decoded".
INSERT INTO decodable_ignorables VALUES

-- Generated as "BIT $#xx", replaced by "JSR $xxxx"
(0x20),

-- Generated as "STY #xx", not implemented on the 6502, replaced by
-- "BRA #r" on the 65C02
(0x80),

-- Generated as "STA #xx", not implemented on the 6502, replaced by
-- "BIT #xx" on the 65C02
(0x89),

-- Generated as "STX" (to the accumulator), replaced by "TXA" which is
-- basically the same thing, but traditionally treated as a separate
-- instruction.
(0x8A),

-- Generated as "STX $xx,X", replaced by "STX $xx,Y"
(0x96),

-- Generated as "STY $xxxx,X", not implemented on the 6502, replaced by
-- "STZ $xxxx" on the 65C02
(0x9C),

-- Generated as "STX $xxxx,X", not implemented on the 6502, replaced by
-- "STZ $xxxx,X" on the 65C02
(0x9E),

-- Generated as "LDX" (from the accumulator), replaced by "TAX" which
-- is basically the same thing, but traditionally treated as a separate
-- instruction.
(0xAA),

-- Generated as "LDX $xx,X", replaced by "LDX $xx,Y"
(0xB6),

-- Generated as "LDX $xxxx,X", replaced by "LDX $xxxx,Y"
(0xBE),

-- Generated as "DEC" (in the accumulator), replaced by DEX.
(0xCA),

-- Generated as "INC" (in the accumulator), replaced by NOP.
(0xEA),

-- Generated as "CPY $xx,X", not implemented on the 6502 or 65C02
(0xD4),

-- Generated as "CPY $xxxx,X", not implemented on the 6502 or 65C02
(0xDC),

-- Generated as "CPX $xx,X", not implemented on the 6502 or 65C02
(0xF4),

-- Generated as "CPX $xxxx,X", not implemented on the 6502 or 65C02
(0xFC);

-- Now that we have all the information we need about decodable opcodes,
-- along with citations for those details, we can generate the complete
-- list of decodable opcodes, along with the instruction and addressing
-- mode each one decodes to.
CREATE VIEW decodable_opcodes AS
SELECT
    -- The original opcode is reconstructed by using bitwise operations to
    -- merge the instruction code, addressing mode code, and group code.
    ((di.code << 5) | (dam.code << 2) | opcode_group) AS opcode,
    -- We get the mnemonic and mode from the source tables
    mnemonic,
    discriminator,
    mode
FROM decodable_instructions AS di
    JOIN decodable_addressing_modes AS dam USING (opcode_group)
-- Don't forget to filter out the opcodes we don't want to generate!
WHERE opcode NOT IN (SELECT opcode FROM decodable_ignorables)
;

-- Hardcoded opcodes
--------------------
--
-- Now that we've generated all the opcodes that can automatically be
-- generated, we need to add all the one-off, special-case opcodes which
-- turn out to be the most important - branches, stack manipulation,
-- subroutines, etc.
CREATE TABLE hardcoded_opcodes (

    -- The opcode whose decoding we are recording
    opcode INTEGER NOT NULL PRIMARY KEY
        CHECK (opcode >= 0 AND opcode <= 255),

    -- The instruction this opcode executes
    mnemonic TEXT NOT NULL,
    discriminator TEXT NOT NULL,

    -- The addressing mode this opcode uses
    mode TEXT NOT NULL REFERENCES addressing_modes,

    -- We want to validate that the kind of this instruction is compatible
    -- with the mode.  Ideally we'd have a CHECK constraint that looks
    -- them up in the instructions and modes_for_instruction_kind tables,
    -- but we are not allowed to have a SELECT in a CHECK constraint.
    --
    -- Instead, we'll store the instruction_kind as a field in this
    -- table, and use foreign key constraints to enforce that it matches
    -- the value in the instructions table, and that the kind and mode
    -- appear in the modes_for_instruction_kind table.
    instruction_kind TEXT NOT NULL,

    -- Verify that this record references an actual instruction that
    -- exists, along with the correct kind.
    FOREIGN KEY (mnemonic, discriminator, instruction_kind)
        REFERENCES instructions(mnemonic, discriminator, instruction_kind),

    -- Verify that the instruction_kind and mode are compatible with
    -- each other.
    FOREIGN KEY (instruction_kind, mode)
        REFERENCES modes_for_instruction_kind
) STRICT;

-- This information comes from "Table 5-2 W65C02S OpCode Matrix" in the
-- W65C02S data sheet.
INSERT INTO hardcoded_opcodes VALUES

-- Because of the way the program counter is handled after a BRK (see
-- the discussion in the instructions table above) we give BRK the
-- 'Immediate' addressing mode rather than 'Implied'.
(0x00, 'BRK', '',          'Immediate',              'Read'),

(0x04, 'TSB', '',          'ZeroPage',               'ReadModifyWrite'),
(0x0C, 'TSB', '',          'Absolute',               'ReadModifyWrite'),
(0x08, 'PHP', '',          'Implied',                'Implied'),
(0x10, 'BPL', '',          'ProgramCounterRelative', 'Address'),
(0x12, 'ORA', '',          'ZeroPageIndirect',       'Read'),
(0x14, 'TRB', '',          'ZeroPage',               'ReadModifyWrite'),
(0x18, 'CLC', '',          'Implied',                'Implied'),
(0x1A, 'INC', '',          'Accumulator',            'ReadModifyWrite'),
(0x1C, 'TRB', '',          'Absolute',               'ReadModifyWrite'),
(0x20, 'JSR', '',          'Absolute',               'Address'),
(0x28, 'PLP', '',          'Implied',                'Implied'),
(0x30, 'BMI', '',          'ProgramCounterRelative', 'Address'),
(0x32, 'AND', '',          'ZeroPageIndirect',       'Read'),
(0x38, 'SEC', '',          'Implied',                'Implied'),
(0x3A, 'DEC', '',          'Accumulator',            'ReadModifyWrite'),
(0x40, 'RTI', '',          'Implied',                'Implied'),
(0x48, 'PHA', '',          'Implied',                'Implied'),
(0x4C, 'JMP', '',          'Absolute',               'Address'),
(0x50, 'BVC', '',          'ProgramCounterRelative', 'Address'),
(0x52, 'EOR', '',          'ZeroPageIndirect',       'Read'),
(0x58, 'CLI', '',          'Implied',                'Implied'),
(0x5A, 'PHY', '',          'Implied',                'Implied'),
(0x60, 'RTS', '',          'Implied',                'Implied'),
(0x64, 'STZ', '',          'ZeroPage',               'Write'),
(0x68, 'PLA', '',          'Implied',                'Implied'),
(0x6C, 'JMP', 'indirect',  'Absolute',               'Address'),
(0x70, 'BVS', '',          'ProgramCounterRelative', 'Address'),
(0x72, 'ADC', '',          'ZeroPageIndirect',       'Read'),
(0x74, 'STZ', '',          'ZeroPageIndexedWithX',   'Write'),
(0x78, 'SEI', '',          'Implied',                'Implied'),
(0x7A, 'PLY', '',          'Implied',                'Implied'),
(0x7C, 'JMP', 'indirect',  'AbsoluteIndexedWithX',   'Address'),
(0x80, 'BRA', '',          'ProgramCounterRelative', 'Address'),
(0x88, 'DEY', '',          'Implied',                'Implied'),
(0x89, 'BIT', 'immediate', 'Immediate',              'Read'),
(0x8A, 'TXA', '',          'Implied',                'Implied'),
(0x90, 'BCC', '',          'ProgramCounterRelative', 'Address'),
(0x92, 'STA', '',          'ZeroPageIndirect',       'Write'),
(0x96, 'STX', '',          'ZeroPageIndexedWithY',   'Write'),
(0x98, 'TYA', '',          'Implied',                'Implied'),
(0x9A, 'TXS', '',          'Implied',                'Implied'),
(0x9C, 'STZ', '',          'Absolute',               'Write'),
(0x9E, 'STZ', '',          'AbsoluteIndexedWithX',   'Write'),
(0xA2, 'LDX', '',          'Immediate',              'Read'),
(0xA8, 'TAY', '',          'Implied',                'Implied'),
(0xAA, 'TAX', '',          'Implied',                'Implied'),
(0xB0, 'BCS', '',          'ProgramCounterRelative', 'Address'),
(0xB2, 'LDA', '',          'ZeroPageIndirect',       'Read'),
(0xB6, 'LDX', '',          'ZeroPageIndexedWithY',   'Read'),
(0xB8, 'CLV', '',          'Implied',                'Implied'),
(0xBA, 'TSX', '',          'Implied',                'Implied'),
(0xBE, 'LDX', '',          'AbsoluteIndexedWithY',   'Read'),
(0xC8, 'INY', '',          'Implied',                'Implied'),
(0xCA, 'DEX', '',          'Implied',                'Implied'),
(0xD0, 'BNE', '',          'ProgramCounterRelative', 'Address'),
(0xD2, 'CMP', '',          'ZeroPageIndirect',       'Read'),
(0xD8, 'CLD', '',          'Implied',                'Implied'),
(0xDA, 'PHX', '',          'Implied',                'Implied'),
(0xE8, 'INX', '',          'Implied',                'Implied'),
(0xEA, 'NOP', '',          'Implied',                'Implied'),
(0xF0, 'BEQ', '',          'ProgramCounterRelative', 'Address'),
(0xF2, 'SBC', '',          'ZeroPageIndirect',       'Read'),
(0xF8, 'SED', '',          'Implied',                'Implied'),
(0xFA, 'PLX', '',          'Implied',                'Implied');

-- Puttiing it all together
---------------------------
--
-- Now we have all the opcode information we can generate in
-- decodable_opcodes, and a hard-coded list of all the information we
-- can't generate, so let's put it together into our final opcode list.
CREATE VIEW opcodes AS

-- To make the SQL query a little simpler, we start by merging the
-- hardcoded and decodable opcode lists.  The decodable_ignorables table
-- should ensure that no opcodes appear in both sets.
WITH all_opcodes AS (
    SELECT opcode, mnemonic, discriminator, mode
    FROM hardcoded_opcodes as ho
    UNION ALL
    SELECT opcode, mnemonic, discriminator, mode
    FROM decodable_opcodes as do
),

-- Next, we join all the information and metadata we have about each of
-- these operations.
opcodes_and_metadata AS (
SELECT
    opcode,
    mnemonic,
    discriminator,
    mode,
    instruction_kind,
    ik.cycles as instruction_kind_cycles,
    i.base_cycles as instruction_base_cycles,
    i.extra_cycles as instruction_extra_cycles,
    am.base_cycles as addressing_mode_base_cycles,
    am.extra_cycles as addressing_mode_extra_cycles,
    am.bytes as addressing_mode_bytes,
    pattern,

    -- The 6502 Instruction Set Decoded says, "Absolute,X, absolute,Y,
    -- and (zero page),Y addressing modes need an extra cycle if the
    -- indexing crosses a page boundary, or if the instruction writes
    -- to memory."  It also says of the 65C02 that "ASL, LSR, ROL,
    -- and ROR, in absolute,X addressing mode, don't pay the one-cycle
    -- penalty that other write instructions pay, unless the indexing
    -- crosses a page boundary.  (Most manufacturer data sheets wrongly
    -- state that this change affects INC and DEC too.  In fact INC and
    -- DEC have exactly the same timing as on the original 6502.)"
    --
    -- Combining these rules, if this operation is a write instruction...
    instruction_kind LIKE '%Write%'
        -- ...and the addressing mode is one of those listed...
        AND mode IN (
            'AbsoluteIndexedWithX',
            'AbsoluteIndexedWithY',
            'ZeroPageIndirectIndexedWithY'
        )
        -- ...and the instruction is NOT one of those that was modified
        -- in the 65C02 (it says "in absolute,X addressing mode", but
        -- these instructions do not occur in any of the other affected
        -- addressing modes, so we don't need to check that)
        AND mnemonic NOT IN ('ASL', 'LSR', 'ROL', 'ROR')
        -- ... then this instruction always pays the page crossing
        -- penalty.
        as always_pays_page_crossing_penalty

FROM all_opcodes
    JOIN instructions AS i USING (mnemonic, discriminator)
    JOIN instruction_kinds AS ik USING (instruction_kind)
    JOIN addressing_modes AS am USING (mode)
)

-- Now, we can combine all that data to produce the information we need.
SELECT
    opcode,
    mnemonic,
    discriminator,
    mode,

    -- This is the minimum number of cycles this opcode takes to execute.
    CASE

        -- If this instruction always pays the page-crossing penalty,
        -- then include the addressing mode's extra cycles in the base
        -- cycle count.
        WHEN always_pays_page_crossing_penalty
        THEN
            instruction_kind_cycles
            + instruction_base_cycles
            + addressing_mode_base_cycles
            + addressing_mode_extra_cycles

        -- When used with the accumulator, ReadModifyWrite instructions
        -- do not incur the cycle penalties for extra memory accesses,
        -- so we completely override the cycle count.
        WHEN instruction_kind = 'ReadModifyWrite'
            AND mode = 'Accumulator'
        THEN 2

        -- Otherwise, the minimum cycle count for this operation is the
        -- sum of the counts for its component parts..
        ELSE
            instruction_kind_cycles
            + instruction_base_cycles
            + addressing_mode_base_cycles
    END as base_cycles,

    -- This is the number of optional extra cycles this operation may
    -- require.
    CASE

        -- If this instruction always pays the page-crossing penalty,
        -- we handled the addressing mode's extra cycles in the base
        -- cycle count, so don't include it here.
        WHEN always_pays_page_crossing_penalty
        THEN instruction_extra_cycles

        -- All the references I've been using have a note beside each
        -- use of the AbsoluteIndexedWithX addressing mode, which says
        -- something like "pays an extra cycle penalty when the indexing
        -- crosses a page boundary".  Each use, that is, other than JMP
        -- ($hhhh,X).
        --
        -- At first I thought it might be a typo or an OCR error.
        -- It seems to break a basic rule of the AbsoluteIndexedWithX
        -- addressing mode, so if it was intended, I expected it to be
        -- called out explicitly.  Instead, it was allowed to pass without
        -- comment in every single reference I looked at; all the ones in
        -- the bibliography below, and various others besides.  I wondered
        -- if perhaps they all copied from the same mistaken source, but
        -- having given it some thought, I think it actually makes sense.
        --
        -- The original 6502 has a "bug" with indirect JMPs where, after
        -- it increments the low byte of the given address, it doesn't
        -- propagate the carry bit to the high byte.  If the low byte
        -- of the target address is stored at $12FF, it reads the high
        -- byte of the target from $1200 instead of $1300.
        --
        -- The 65C02 fixes this behaviour by adding a guaranteed fixup
        -- cycle.  After incrementing the low byte, it always spends a
        -- cycle handling the carry bit, whether the carry bit is set
        -- or clear.
        --
        -- The AbsoluteIndexedWithX addressing mode has a similar issue.
        -- It adds the X register to the low byte of the given address,
        -- and if there's an overflow it takes an extra cycle (the "page
        -- crossing" penalty) to propagate the carry bit to the high byte.
        --
        -- In "JMP ($1234,X)" we get both situations: adding X could cause
        -- an overflow, and then incrementing could cause an overflow.
        -- But only one of those can occur!  If adding X causes overflow,
        -- incrementing cannot.  If incrementing causes overflow, adding
        -- X cannot.  Either way, the indirect JMP's guaranteed fixup
        -- cycle fixes it, and there's no need for the addressing mode
        -- to pay an additional page-crossing penalty.
        --
        -- Since the addressing mode's extra cycle is absorbed into
        -- the base cycle count, we'll remove the addressing mode's
        -- extra_cycles from the calculation here.
        WHEN discriminator = 'indirect'
            AND mode LIKE '%Indexed%'
            THEN instruction_extra_cycles

        -- Otherwise, this instruction's extra cycles can come from both
        -- the instruction and the addressing mode.
        ELSE instruction_extra_cycles + addressing_mode_extra_cycles

    END as extra_cycles,

    -- The number of bytes this instruction takes
    addressing_mode_bytes as bytes,

    -- A human-readable representation of this instruction.  See the
    -- documentation for addressing_modes.pattern for details.
    CASE
        WHEN discriminator = 'indirect' THEN
            mnemonic || ' (' || pattern || ')'
        WHEN pattern <> '' THEN
            mnemonic || ' ' || pattern
        ELSE mnemonic
    END AS pattern
FROM opcodes_and_metadata
;

-- Bibliography
---------------
--
-- This information has been compiled from a number of sources,
-- in particular:
--
--   - The W65C02S data sheet, dated 2022-04-08, from
--     https://www.westerndesigncenter.com/wdc/documentation/w65c02s.pdf
--   - "The 6502 Instruction Set Decoded", dated 2022-04-24, from
--     https://llx.com/Neil/a2/opcodes.html
--   - The Synertek Programming Manual, dated 1976-08 (on the first page)
--     or 1978-05 (on the spine), from
--     https://archive.org/details/Synertek_Programming_Manual/
--   - "65C02 Opcodes", date 2023-01-07, from
--     http://www.6502.org/tutorials/65c02opcodes.html
