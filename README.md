The 6502 Instruction Set Database
=================================

The 6502 instruction set is a gem of economy,
allowing complex programs to be written and execute quickly
while using very few transistors
in a simple-to-fabricate pattern.
Unfortunately,
that means it can be a little quirky and has a bunch of corner-cases,
so if you're trying to implement the 6502 yourself
or trying to verify an implementation,
it can be difficult to be sure it's all correct.

There are many websites that list 6502 instructions and cycle counts,
but many of them seem to be derived from each other,
and since they're generally just big tables,
who's to know whether they may contain typos
or OCR errors?
I didn't want to copy somebody else's table and have to trust it,
I wanted to understand it
and be able to easily verify it against different sources.

Instead, I hatched a plan:

 1. Collect sources, and encoded the raw data in a machine-readable form
 2. Study those sources, and encode my understanding
    as assertions, sanity-checks and validations of that data
 3. Synthesise that data according to my understanding,
    and verify it against the sources available

In the end,
instead of having a bunch of different sources that might disagree
and no way to know which is more correct,
I will hopefully have some sources that are correct,
and other sources that are incorrect for specific, plausible reasons.

At first,
I started doing this in a procedural programming language,
but once I realised I was declaring data
and declaring constraints on that data,
I realised I should be using a declarative data language.
I'm sure there's some really good languages
custom built for this kind of knowledge-representation,
but SQL is the language I know,
so that's what I used.

How to use it
=============

The primary use is to read [isa65xx.sql](./isa65xx.sql)
and learn about the structures underlying the 6502 instruction set.

If you just want the result without recalculating it yourself,
the [opcodes_expected.tsv](./opcodes_expected.tsv) file
contains the "known good" output the test suite checks against.
See the definition of the `opcodes` view in [isa65xx.sql](./isa65xx.sql)
for a description of each field.

To run the included tests and validations, you will need:

  - A POSIXy environment with a shell, `rm`, and `diff`
  - The `sqlite3` command-line tool, version 3.37.0 or higher
      - If you delete the `STRICT` keyword from the SQL file,
        it should work with earlier versions
        but you will lose the correctness validation it provides

Run the included [build.sh](./build.sh) script.
If it encounters a problem,
it will print a message
and exit with a non-zero exit code.
